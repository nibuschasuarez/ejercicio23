import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-padre',
  templateUrl: './padre.component.html',
  styleUrls: ['./padre.component.css']
})
export class PadreComponent implements OnInit {

  padretexto="compra pan, leche y queso"
  textopadre2="regresa a la tienda y cambia el pan por huevo"

  listacompras = {
    frutas:'manzana, pera, sandia,platano',
    verduras:'lechuga, papa, tomate',
    abarrotes:'arroz,azucar'
  }

  compramaterialescolar = {
    cuadernos:'empastado,anillado,carta, oficio',
    boligrafo:'rojo,verde,azul,negro',
    mochila:'totto,adidas'
  }

  constructor() {
  
   }

  ngOnInit(): void {
  }

}
